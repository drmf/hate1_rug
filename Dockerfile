FROM python:3.6-slim

COPY ./ /elg/

WORKDIR /elg

RUN python -m venv venv && venv/bin/pip install --upgrade pip && venv/bin/pip --no-cache-dir install -r requirements.txt && venv/bin/python -m nltk.downloader stopwords

ENV WORKERS=1

ENTRYPOINT ["./docker-entrypoint.sh"]
