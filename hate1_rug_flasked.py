import pickle

import numpy as np
from flask import Flask, request
from flask_json import FlaskJSON, JsonError, as_json
import keras.models
import pandas as pd

import data_helpers_TW

app = Flask(__name__)
# Don't add an extra "status" property to JSON responses - this would break the API contract
app.config['JSON_ADD_STATUS'] = False
# Don't sort properties alphabetically in response JSON
app.config['JSON_SORT_KEYS'] = False

json_app = FlaskJSON(app)


@json_app.invalid_json_error
def invalid_request_error(e):
    """Generates a valid ELG "failure" response if the request cannot be parsed"""
    raise JsonError(status_=400, failure={'errors': [
        {'code': 'elg.request.invalid', 'text': 'Invalid request message'}
    ]})


@app.route('/process', methods=['POST'])
@as_json
def process_request():
    """Main request processing logic - accepts a JSON request and returns a JSON response."""
    data = request.get_json()
    # sanity checks on the request message
    if data.get('type') == 'text':
        # single item
        if 'content' not in data:
            invalid_request_error(None)

        content = data['content']
        result = do_nlp(content)
        return dict(response={'type': 'classification', 'classes': result})
    elif data.get('type') == 'structuredText':
        if ('texts' not in data) or any('content' not in t for t in data['texts']):
            invalid_request_error(None)

        content = [t['content'] for t in data['texts']]
        result = do_nlp(content)
        # result is a list of {'class':'xxx'}, parallel to the input 'texts' list, so
        # represent the output as "annotations" where the classification for text N
        # (zero-based index) is an annotation from N to N+1
        return dict(response={
            'type': 'annotations',
            'annotations':{
                'classes':[{'start': i, 'end':i+1, 'features': val} for (i, val) in enumerate(result)]
            }
        })
    else:
        invalid_request_error(None)


@app.before_first_request
def load_models():
    # load classifier
    global model
    model = keras.models.load_model('cnn_tw.model')
    # load prebuilt vocabulary
    global vocabulary
    vocabulary = pd.read_pickle('vocabulary.pickle')


def label_name(x):
    if int(x) == 0:
        return 'not hate speech'
    return 'hate speech'


def do_nlp(content):
    # n samples, two labels
    if type(content) is str:
        content = [content]
    X = [data_helpers_TW.clean_str(sample) for sample in content]
    X = [sample.split() for sample in X]
    sentences_padded = data_helpers_TW.pad_sentences(X, sequence_length=79)
    # replace OOV words with "neutral" word
    X = np.array([np.array([vocabulary.get(word, vocabulary['giorno']) for word in sentence]) for sentence in sentences_padded])
    classes = []
    y = model.predict(X)
    results = []
    for sample in range(len(sentences_padded)):
        prob_1 = y[sample][0]
        prob_0 = 1 - prob_1
        max_label = 1 if prob_1 >= prob_0 else 0
        results.append({'class': label_name(max_label)})
    return results


if __name__ == '__main__':
    app.run()
